const menuBtn = document.querySelector('.nav-menu-toggle');
const menu = document.querySelector('.nav-menu');
const icons = menuBtn.querySelectorAll('i');

menuBtn.addEventListener('click', () => {
    menu.classList.toggle('hidden');
    icons.forEach(icon => {
        icon.classList.toggle('hidden');
    });
});