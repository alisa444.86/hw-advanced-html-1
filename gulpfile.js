const gulp = require('gulp');
const sass = require('gulp-sass');
const clean = require('gulp-clean');
const autoPrefixer = require('gulp-autoprefixer');
const cleanCss = require('gulp-clean-css');
const imageMin = require('gulp-imagemin');
const concat = require('gulp-concat');
const minify = require('gulp-js-minify');
const browserSync = require('browser-sync').create();
const uglify = require('gulp-uglify');

const cleanDist = () => {
    return gulp.src('dist', {read: false, allowEmpty: true})
        .pipe(clean());
};

const scssBuild = () => {
    return gulp.src('src/scss/*.scss')
        .pipe(sass())
        .pipe(autoPrefixer())
        .pipe(cleanCss({compatibility: '1e8'}))
        .pipe(gulp.dest('dist/scss/'));
};

const jsBuild = () => {
    return gulp.src('src/js/*.js')
        .pipe(concat('main.js'))
        .pipe(minify())
        .pipe(uglify())
        .pipe(gulp.dest('dist/js/'));
};

const imgOptimization = () => {
    return gulp.src('src/img/**/*.*')
        .pipe(imageMin())
        .pipe(gulp.dest('dist/img/'));
};

const watch = () => {
    gulp.watch('src/scss/*.scss', scssBuild).on('change', browserSync.reload);
    gulp.watch('src/img/*').on('change', browserSync.reload);
    gulp.watch('src/js/*.js', jsBuild).on('change', browserSync.reload);
    gulp.watch('index.html').on('change', browserSync.reload);
    browserSync.init({
        server: {
            baseDir: './'
        }
    });

};

gulp.task('build', gulp.series(cleanDist, scssBuild, jsBuild, imgOptimization));
gulp.task('dev', watch);


